package main

import (
	"community_tools/joltify"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"github.com/ilyakaznacheev/cleanenv"
	"github.com/rs/zerolog"
	"os"
	"strings"
)

func prepareEnvironment() joltify.ConfigFaucet {
	var cfg joltify.ConfigFaucet
	err := cleanenv.ReadConfig(".config.yml", &cfg)
	if err != nil {
		panic(err)
	}
	return cfg
}

func main() {

	bytesRead, err := os.ReadFile("/home/yb/development/joltify/joltify-community/cmd/role_remove/list.txt")
	if err != nil {
		fmt.Printf("error in read file %v", err)
		return
	}
	fileContent := string(bytesRead)
	lines := strings.Split(fileContent, "\n")

	logger := zerolog.New(os.Stdout).With().Str("service", "main").Timestamp().Logger()
	cfg := prepareEnvironment()

	token := cfg.DisCordToken
	if token == "" {
		logger.Error().Msg("no token provided")
		return
	}

	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + token)
	if err != nil {
		logger.Error().Err(err).Msgf("fail to create discord session")
		return
	}

	dg.Identify.Intents = discordgo.IntentsGuilds | discordgo.IntentsGuildMessages | discordgo.IntentGuildMembers

	guildID := "905288535228158052"
	earlyAdopter := "1161973521883340880"

	processed := 0
	for _, userID := range lines {
		err = dg.GuildMemberRoleRemove(guildID, userID, earlyAdopter)
		if err != nil {
			logger.Error().Err(err).Msg("fail to remove role")
			continue
		}
		fmt.Printf(">remove role for user %v\n", userID)
		processed++
	}

	fmt.Printf(">>we have processed>>%v\n", processed)

}
