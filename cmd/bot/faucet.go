package main

import (
	"community_tools/grpc"
	"community_tools/joltify"
	types2 "community_tools/types"
	"context"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"github.com/cosmos/cosmos-sdk/types"
	"github.com/ilyakaznacheev/cleanenv"
	"github.com/rs/zerolog/log"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func prepareEnvironment() joltify.ConfigFaucet {
	var cfg joltify.ConfigFaucet
	err := cleanenv.ReadConfig(".config.yml", &cfg)
	if err != nil {
		panic(err)
	}
	return cfg
}

func setupbech32() {
	config := types.GetConfig()
	config.SetBech32PrefixForAccount("jolt", "joltpub")
	config.SetBech32PrefixForValidator("joltval", "joltvpub")
	config.SetBech32PrefixForConsensusNode("joltvalcons", "joltcpub")
}

func main() {
	setupbech32()
	cfg := prepareEnvironment()
	faucet, err := joltify.InitFaucet(cfg)
	if err != nil {
		fmt.Printf("error in init faucet %v", err)
		return
	}

	node, err := grpc.NewCosOperations(cfg.HttpAddr)
	if err != nil {
		fmt.Printf("error in init grpc %v", err)
		return
	}

	allAuctions, err := faucet.QueryAuctionAll()
	if err != nil {
		fmt.Printf("error in query all auctions %v", err)
		return
	}

	//ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	ctx, cancel := context.WithCancel(context.Background())
	for _, el := range allAuctions.Auctions {
		err := node.AddSubscribe(ctx, el.BaseAuction.ID)
		if err != nil {
			fmt.Printf("error in add subscription %v", err)
			continue
		}
	}

	token := cfg.DisCordToken
	if token == "" {
		log.Error().Msg("no token provided")
		return
	}

	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + token)
	if err != nil {
		log.Error().Err(err).Msgf("fail to create discord session")
		return
	}

	dg.AddHandler(faucet.ProcessMessage)
	dg.AddHandler(faucet.ProcessEventAddMember)
	//dg.AddHandler(faucet.ProcessEventRemoveMember)

	dg.Identify.Intents = discordgo.IntentsGuilds | discordgo.IntentsGuildMessages | discordgo.IntentsGuildVoiceStates | discordgo.IntentGuildMembers

	// Open the websocket and begin listening.
	err = dg.Open()
	if err != nil {
		log.Error().Err(err).Msgf("fail to open discord session")
		return
	}

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("wallet faucet is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)

	for {
		select {
		case <-sc:
			// Cleanly close down the Discord session.
			err = dg.Close()
			if err != nil {
				log.Error().Err(err).Msgf("fail to clsoe the discord session")
			}

			err = faucet.CloseDB()
			if err != nil {
				log.Error().Err(err).Msgf("fail to close the db")
				return
			}
			cancel()
			err = node.TerminateCosmosClient()
			if err != nil {
				log.Error().Err(err).Msgf("fail to terminate the cosmos client")
			}

			node.ChannelWg.Wait()
			return

		case msg := <-node.BidChan:
			auctionID, ok := msg.Events["auction_bid.auction_id"]
			if !ok {
				log.Error().Msgf("fail to get auction id from event")
				continue
			}

			detail, err := faucet.QueryAuctionDetail(auctionID[0])
			if err != nil {
				log.Error().Err(err).Msgf("fail to query auction detail")
				continue
			}

			a6, ok := types.NewIntFromString("1000_000")
			if !ok {
				panic("fail to convert the amount")
			}
			a18, ok := types.NewIntFromString("1000_000_000_000_000_000")
			if !ok {
				panic("fail to convert the amount")
			}

			amountJolt := types.MustNewDecFromStr(detail.Data.BaseAuction.Bid.Amount)

			amountLot := types.MustNewDecFromStr(detail.Data.BaseAuction.Lot.Amount)

			humanJolt := amountJolt.QuoInt(a6)
			humanLot := amountLot.QuoInt(a18)

			bidStr := humanJolt.String() + detail.Data.BaseAuction.Bid.Denom
			lotStr := humanLot.String() + detail.Data.BaseAuction.Lot.Denom

			finalEndTime := detail.Data.BaseAuction.EndTime
			if detail.Data.BaseAuction.EndTime.After(detail.Data.BaseAuction.MaxEndTime) {
				finalEndTime = detail.Data.BaseAuction.MaxEndTime
			}

			err = faucet.StoreAuction(detail)
			if err != nil {
				log.Error().Err(err).Msgf("fail to store the auction")
				continue
			}

			timeGap := finalEndTime.Sub(time.Now())

			markedBidder := detail.Data.BaseAuction.Bidder[:9] + "****" + detail.Data.BaseAuction.Bidder[len(detail.Data.BaseAuction.Bidder)-4:]
			msgS := fmt.Sprintf("```css\n Auction %v has updated by bidder **%v (%v)** for %v, auction ends in %v hurry up!!```", auctionID, markedBidder, bidStr, lotStr, timeGap.String())
			_, err = dg.ChannelMessageSend(types2.BIDCHANNEL, msgS)
			if err != nil {
				log.Error().Err(err).Msgf("fail to send message")
			}

		case msg := <-node.CloseBidChan:
			auctionID, ok := msg.Events["auction_close.auction_id"]
			if !ok {
				log.Error().Msgf("fail to get auction id from event")
				continue
			}
			detail, err := faucet.QueryAuctionAndDelete(auctionID[0])
			if err != nil {
				log.Error().Err(err).Msgf("fail to query auction id", auctionID)
				continue
			}
			err = node.RemoveSubscribe(ctx, detail.Data.BaseAuction.ID)
			if err != nil {
				log.Error().Err(err).Msgf("fail to remove subscription")
				continue
			}

			amountJolt := types.MustNewDecFromStr(detail.Data.BaseAuction.Bid.Amount)

			amountLot := types.MustNewDecFromStr(detail.Data.BaseAuction.Lot.Amount)
			a6, ok := types.NewIntFromString("1000_000")
			if !ok {
				panic("fail to convert the amount")
			}
			a18, ok := types.NewIntFromString("1000_000_000_000_000_000")
			if !ok {
				panic("fail to convert the amount")
			}

			humanJolt := amountJolt.QuoInt(a6)
			humanLot := amountLot.QuoInt(a18)

			bidStr := humanJolt.String() + detail.Data.BaseAuction.Bid.Denom
			lotStr := humanLot.String() + detail.Data.BaseAuction.Lot.Denom

			markedBidder := detail.Data.BaseAuction.Bidder[:9] + "****" + detail.Data.BaseAuction.Bidder[len(detail.Data.BaseAuction.Bidder)-4:]
			msgS := fmt.Sprintf("```css\n Auction %v has been taken by bidder **%v (%v)** for %v```", auctionID, markedBidder, bidStr, lotStr)

			_, err = dg.ChannelMessageSend(types2.BIDCHANNEL, msgS)
			if err != nil {
				log.Error().Err(err).Msgf("fail to send message")
			}

		case <-time.After(20 * time.Minute):
			faucet.Collect(nil, nil)
		}

	}

}
