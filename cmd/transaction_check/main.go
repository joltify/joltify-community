package main

import (
	"fmt"
	"strconv"

	"community_tools/joltify"
	"github.com/cosmos/cosmos-sdk/types"
	"github.com/ilyakaznacheev/cleanenv"
	"github.com/rs/zerolog/log"
)

func setupbech32() {
	config := types.GetConfig()
	config.SetBech32PrefixForAccount("jolt", "joltpub")
	config.SetBech32PrefixForValidator("joltval", "joltvpub")
	config.SetBech32PrefixForConsensusNode("joltvalcons", "joltcpub")
}

func prepareEnvironment() joltify.ConfigFaucet {
	var cfg joltify.ConfigFaucet
	err := cleanenv.ReadConfig(".config.yml", &cfg)
	if err != nil {
		panic(err)
	}
	return cfg
}

func main() {

	setupbech32()
	cfg := prepareEnvironment()
	faucet, err := joltify.InitFaucet(cfg)
	if err != nil {
		fmt.Printf("error in init faucet %v", err)
		return
	}

	height, err := faucet.QueryHeightRecord()
	if err != nil {
		fmt.Printf("error in query height record %v", err)
		return
	}

	var currentHeight uint64
	if height != nil {
		currentHeight, err = strconv.ParseUint(string(height), 10, 64)
	}

	txWithAccount, maxHeight, err := faucet.QueryUserTx(currentHeight)
	if err != nil {
		log.Error().Msgf("error in query user tx %v", err)
		return
	}

	faucet.ProcessTxAndUpdate(txWithAccount)

	err = faucet.CloseDB()
	if err != nil {
		fmt.Printf("error in close db %v", err)
		return
	}

	log.Info().Msgf("current height is %v and max height we processed %v", currentHeight, maxHeight)

}
