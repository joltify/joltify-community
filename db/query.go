package db

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

type HasuraResponse struct {
	Data   interface{} `json:"data"`
	Errors []struct {
		Message string `json:"message"`
	} `json:"errors"`
}

func QueryTxFromHasura(url, query string) (interface{}, error) {
	// Create a JSON payload for the GraphQL request
	requestBody, err := json.Marshal(map[string]interface{}{
		"query": query,
	})
	if err != nil {
		fmt.Println("Failed to marshal JSON request:", err)
		return nil, err
	}

	// Send the GraphQL request to Hasura
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(requestBody)) //nolint
	if err != nil {
		fmt.Println("Failed to send GraphQL request:", err)
		return nil, err
	}
	defer resp.Body.Close()

	// Parse the response
	var hasuraResponse HasuraResponse
	err = json.NewDecoder(resp.Body).Decode(&hasuraResponse)
	if err != nil {
		fmt.Println("Failed to decode JSON response:", err)
		return nil, err
	}

	// Check for errors in the response
	if len(hasuraResponse.Errors) > 0 {
		for _, err := range hasuraResponse.Errors {
			fmt.Println("GraphQL error:", err.Message)
		}
		return nil, err
	}

	return hasuraResponse.Data, nil
}
