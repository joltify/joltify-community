package db

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"sync"
	"time"

	types2 "community_tools/types"
	"github.com/cosmos/cosmos-sdk/types"
	"github.com/syndtr/goleveldb/leveldb"
)

const (
	maxWallet = 10000
	timeGap   = 2
)

type LevelDB struct {
	db     *leveldb.DB
	locker *sync.RWMutex
}

func OpenDB() LevelDB {
	db, err := leveldb.OpenFile("./user.db", nil)
	if err != nil {
		log.Fatal("fail to open the db!")
	}
	return LevelDB{db, &sync.RWMutex{}}
}

func (l *LevelDB) CloseDB() error {
	err := l.db.Close()
	return err
}

func (l *LevelDB) Delete(key []byte) error {
	l.locker.Lock()
	defer l.locker.Unlock()
	return l.db.Delete(key, nil)
}

func (l *LevelDB) WriteDB(key, value []byte) error {
	l.locker.Lock()
	defer l.locker.Unlock()
	return l.db.Put(key, value, nil)
}

func (l *LevelDB) QueryDB(key []byte) ([]byte, error) {
	l.locker.RLock()
	data, err := l.db.Get(key, nil)
	l.locker.RUnlock()
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (l *LevelDB) ValidationCheck(userID string, addr types.AccAddress) (bool, error) {
	value, err := l.QueryDB([]byte(userID))
	// query for wallet info
	storedDiscordID, err2 := l.QueryDB(addr.Bytes())
	if err != nil {
		if strings.Contains(err.Error(), "leveldb: not found") {
			if err2 != nil {
				if strings.Contains(err.Error(), "leveldb: not found") {
					return true, nil
				}
				return false, err
			}
			if err2 == nil {
				return false, fmt.Errorf("the address has been used by user %v", string(storedDiscordID))
			}
		}
		return false, err
	}

	var walletsInfo types2.WalletEntry
	err = json.Unmarshal(value, &walletsInfo)
	if err != nil {
		return false, err
	}

	storedUserID, err := l.QueryDB(addr.Bytes())
	if err != nil && (!strings.Contains(err.Error(), "leveldb: not found")) {
		return false, err
	}

	if len(storedUserID) != 0 && string(storedUserID) != userID {
		return false, fmt.Errorf("the address has been used by user %v", string(storedUserID))
	}

	earliestTime := walletsInfo.Timestamp.Add(time.Minute * timeGap)

	timeGap := time.Until(earliestTime)

	if time.Now().Before(earliestTime) {
		return false, fmt.Errorf("submit too early, come back in **%v**", timeGap)
	}
	if len(walletsInfo.WalletAddresses) > maxWallet {
		return false, fmt.Errorf("reach max wallet number %v", maxWallet)
	}
	return false, nil
}

func (l *LevelDB) UpdateDB(authID string, addr types.AccAddress) error {
	_, err := l.ValidationCheck(authID, addr)
	if err != nil {
		return err
	}

	value, err := l.QueryDB([]byte(authID))
	if err != nil && (!strings.Contains(err.Error(), "leveldb: not found")) {
		return err
	}

	var walletsInfo types2.WalletEntry
	if len(value) != 0 {
		err = json.Unmarshal(value, &walletsInfo)
		if err != nil {
			return err
		}
	}
	// no record found
	if len(walletsInfo.WalletAddresses) == 0 {
		walletsInfo.WalletAddresses = []types.AccAddress{addr}
	} else {
		found := false
		for _, el := range walletsInfo.WalletAddresses {
			if el.Equals(addr) {
				found = true
			}
		}
		if !found {
			walletsInfo.WalletAddresses = append(walletsInfo.WalletAddresses, addr)
		}
	}
	walletsInfo.Timestamp = time.Now()
	walletsInfo.FirstJoin = false

	value, err = json.Marshal(walletsInfo)
	if err != nil {
		return err
	}

	err = l.WriteDB([]byte(authID), value)
	if err != nil {
		return err
	}

	// now we write the address and account pair
	err = l.WriteDB(addr.Bytes(), []byte(authID))
	if err != nil {
		return err
	}
	return nil
}
