package joltify

import (
	"community_tools/db"
	"encoding/json"
	"fmt"
	"github.com/rs/zerolog/log"
	"io"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"

	"community_tools/tools"
	txstatistic "community_tools/tx_statistic"

	"github.com/bwmarrin/discordgo"
	"github.com/cosmos/cosmos-sdk/types"
)

/*

query MyQuery {
  transaction(where: {block: {height: {_gt: "0", _lte: "100"}}}, order_by: {block: {height: desc}}) {
    messages
    signer_infos
  }
}

http://202.182.98.24:8221/console

*/

func (f *Faucet) QueryHeightRecord() ([]byte, error) {

	height, err := f.db.QueryDB([]byte("last_height"))
	if err != nil {
		if strings.Contains(err.Error(), "leveldb: not found") {
			return []byte("0"), nil
		}
		return nil, err
	}
	return height, err
}

func (f *Faucet) CloseDB() error {
	return f.db.CloseDB()
}

func (f *Faucet) StoreAuction(msg AuctionDetail) error {
	prefix := "auction_id_"
	data, err := json.Marshal(msg)
	if err != nil {
		return err
	}
	err = f.db.WriteDB([]byte(prefix+msg.Data.BaseAuction.ID), data)
	return err
}

func (f *Faucet) QueryAuctionAndDelete(auctionID string) (AuctionDetail, error) {
	prefix := "auction_id_"
	data, err := f.db.QueryDB([]byte(prefix + auctionID))
	if err != nil {
		return AuctionDetail{}, err
	}
	var msg AuctionDetail
	err = json.Unmarshal(data, &msg)
	if err != nil {
		return AuctionDetail{}, err
	}

	err = f.db.Delete([]byte(prefix + auctionID))
	if err != nil {
		panic(err)
	}

	return msg, nil
}

func (f *Faucet) ProcessTxAndUpdate(txMap map[string][]types.AccAddress) {
	for txType, addrs := range txMap {
		for _, addr := range addrs {
			discordID, err := f.db.QueryDB(addr.Bytes())
			if err != nil {
				//log.Warn().Msgf("error in query the discord id %v", err)
				continue
			}
			if len(discordID) == 0 {
				continue
			}
			log.Info().Msgf(">>>we store %v for %v", string(discordID), txType)
			err = f.StoreDiscordUserAction(discordID, txType)
			if err != nil {
				fmt.Printf("error in store the discord id %v", err)
				continue
			}
		}
	}
}

func (f *Faucet) QueryUserTx(startBlock uint64) (map[string][]types.AccAddress, uint64, error) {

	queryTemplate := `
	query MyQuery {
  		transaction(where: {success: {_eq: true}, block: {height: {_gt: %v}}}, order_by: {block: {height: desc}}) {
		messages
    	signer_infos
    	height
  		}
	}`

	query := fmt.Sprintf(queryTemplate, startBlock)

	ret, err := db.QueryTxFromHasura(f.hasuraUrl, query)
	if err != nil {
		fmt.Printf("fail to query the account info")
	}

	txInfo, err := txstatistic.DecodeTx(ret)
	if err != nil {
		return nil, 0, err
	}

	//for _, tx := range txInfo.Transaction {
	//	jsonData, err := json.Marshal(tx.Messages)
	//	if err != nil {
	//		f.logger.Error().Err(err).Msg("failed to marshal json")
	//		continue
	//	}
	//
	//	var txInfoMessageAuction []txstatistic.AuctionMsg
	//	err = json.Unmarshal(jsonData, &txInfoMessageAuction)
	//	if err != nil {
	//		continue
	//	}
	//
	//	for _, msg := range txInfoMessageAuction {
	//		if msg.Type != "/joltify.third_party.auction.v1beta1.MsgPlaceBid" {
	//			continue
	//		}
	//		f.AuctionMsg <- msg
	//	}
	//}

	_, txWithWalletsArray, _, maxHeight, err := txstatistic.TxStatistic(txInfo, "all")
	if err != nil {
		return nil, 0, err
	}

	if maxHeight != 0 {
		err = f.db.WriteDB([]byte("last_height"), []byte(strconv.FormatUint(maxHeight, 10)))
	}
	return txWithWalletsArray, maxHeight, nil

}

func (f *Faucet) doQuery(date, days, txType string) (map[string]int, map[string][]types.AccAddress, map[int][]string, error) {
	layout := "2006-01-02"
	endDate, err := time.Parse(layout, date)
	if err != nil {
		return nil, nil, nil, fmt.Errorf("invalid date, please input the date in format of 2023-09-07")
	}
	dateNum, err := strconv.ParseInt(days, 10, 32)
	if err != nil {
		return nil, nil, nil, fmt.Errorf("invalid duration, please input the days")
	}
	duration := -24 * time.Hour * time.Duration(dateNum)
	startTime := endDate.Add(duration)

	queryTemplate := `
			query MyQuery {
			transaction(where: {success: {_eq: true}, block: {timestamp: {_gt: "%v", _lt: "%v"}}}, order_by: {block: {timestamp: desc}}) {
				messages
	 			signer_infos
				height
			}
		}`

	query := fmt.Sprintf(queryTemplate, startTime.Format(layout), endDate.Format(layout))

	txInfo, err := txstatistic.DecodeTx(query)
	if err != nil {
		return nil, nil, nil, err
	}
	txFreq, txWithWalletsArray, globalFreqAddr, _, err := txstatistic.TxStatistic(txInfo, txType)

	// now we process the txinfos

	return txFreq, txWithWalletsArray, globalFreqAddr, err
}

func (f *Faucet) Ranking(session *discordgo.Session, m *discordgo.MessageCreate) {

	in := strings.Split(m.Content, ":")

	if len(in) != 4 {
		errmsg := " invalid format, pealse use **ranking:<tx type>:<start date(2023-09-07)>:<duration>**"
		tools.SendMsg(session, m, errmsg)
		return
	}

	txType := in[1]

	txFreq, txWithWalletsArray, globalFreqAddr, err := f.doQuery(in[2], in[3], txType)
	if err != nil {
		errMsg := "fail to query the tx statistic" + err.Error()
		tools.SendMsg(session, m, errMsg)
		return
	}

	out := formatTxFreq(txFreq)
	sendMsg := "Hello, " + m.Author.Username + "#" + m.Author.Discriminator + out
	tools.SendMsg(session, m, sendMsg)
	out = formatTxWithWalletsArray(txWithWalletsArray, f.topK)
	sendMsg = "==========================================" + out + "\n==========================================\n"
	tools.SendMsg(session, m, sendMsg)

	out = formatGlobalFreq(globalFreqAddr, f.topK)
	sendMsg = ":first_place:\n==========================================" + out + "\n==========================================\n"
	tools.SendMsg(session, m, sendMsg)
	return
}

func formatGlobalFreq(globalFreqAddr map[int][]string, topK int) string {

	var val []int
	for k, _ := range globalFreqAddr {
		val = append(val, k)
	}
	sort.Slice(val, func(i, j int) bool {
		return val[i] > val[j]
	})
	if topK < len(val) {
		val = val[:topK]
	}

	msgTemplate := "**%v: %v **\n"
	gout := "\n"

	for _, v := range val {
		el := fmt.Sprintf(msgTemplate, v, globalFreqAddr[v])
		gout += el
	}
	return gout
}

func findTopK(addresses []types.AccAddress, topK int) ([]string, map[string]int) {
	freq := make(map[string]int)
	for _, addr := range addresses {
		counter, ok := freq[addr.String()]
		if !ok {
			freq[addr.String()] = 1
		} else {
			freq[addr.String()] = counter + 1
		}
	}

	counterWithAddr := make(map[int][]string)
	for addr, counter := range freq {
		eachAddr, ok := counterWithAddr[counter]
		if !ok {
			counterWithAddr[counter] = []string{addr}
			continue
		}
		eachAddr = append(eachAddr, addr)
		counterWithAddr[counter] = eachAddr
	}

	rankCounter := make([]int, len(counterWithAddr), len(counterWithAddr))
	for c, _ := range counterWithAddr {
		rankCounter = append(rankCounter, c)
	}

	sort.Slice(rankCounter, func(i, j int) bool {
		return rankCounter[i] > rankCounter[j]
	})

	topKElement := rankCounter
	if topK < len(rankCounter) {
		topKElement = rankCounter[:topK]
	}

	var topAddresses []string
	for _, thisFreq := range topKElement {
		a := counterWithAddr[thisFreq]
		topAddresses = append(topAddresses, a...)
	}

	return topAddresses, freq
}

func formatTxWithWalletsArray(in map[string][]types.AccAddress, topK int) string {
	msgTemplate := "**%v: %v **\n"
	gout := "\n"
	for txType, addresses := range in {
		out := ":loudspeaker: **%v**\n"
		out = fmt.Sprintf(out, txType)
		addresses, freqMap := findTopK(addresses, topK)
		for _, el := range addresses {
			out += fmt.Sprintf(msgTemplate, el, freqMap[el])
		}
		gout += out
	}
	return gout
}

func formatTxFreq(freq map[string]int) string {
	msgTemplate := "**%v: %v **\n"
	out := ":loudspeaker:\n"
	for k, v := range freq {
		out += fmt.Sprintf(msgTemplate, k, v)
	}
	return out
}

func (f *Faucet) QueryAuctionDetail(auctionID string) (AuctionDetail, error) {

	var auctionInfo AuctionDetail
	url := fmt.Sprintf("%v/joltify/third_party/auction/v1beta1/auctions/%v", f.restAddr, auctionID)
	// Send an HTTP GET request to the URL
	response, err := http.Get(url)
	if err != nil {
		fmt.Println("Error:", err)
		return auctionInfo, nil
	}
	defer response.Body.Close()

	// Read the response body
	responseBody, err := io.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Error reading response:", err)
		return auctionInfo, nil
	}

	err = json.Unmarshal(responseBody, &auctionInfo)
	if err != nil {
		fmt.Println("Error unmarshal response:", err)
		return auctionInfo, nil
	}
	return auctionInfo, nil
}

func (f *Faucet) QueryAuctionAll() (AllAuctions, error) {
	var allAuctions AllAuctions
	url := fmt.Sprintf("%v/joltify/third_party/auction/v1beta1/auctions", f.restAddr)
	// Send an HTTP GET request to the URL
	response, err := http.Get(url)
	if err != nil {
		fmt.Println("Error:", err)
		return allAuctions, nil
	}
	defer response.Body.Close()

	// Read the response body
	responseBody, err := io.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Error reading response:", err)
		return allAuctions, nil
	}

	err = json.Unmarshal(responseBody, &allAuctions)
	if err != nil {
		fmt.Println("Error unmarshal response:", err)
		return allAuctions, nil
	}
	return allAuctions, nil
}
