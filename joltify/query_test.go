package joltify

import (
	"crypto/rand"
	rand2 "math/rand"
	"testing"

	"github.com/cosmos/cosmos-sdk/crypto/keys/ed25519"
	"github.com/cosmos/cosmos-sdk/types"
	"github.com/magiconair/properties/assert"
)

func TestFindTopK(t *testing.T) {

	pubBz := make([]byte, ed25519.PubKeySize)
	pub := &ed25519.PubKey{Key: pubBz}
	addressPool := make([]string, 20, 20)
	for i := 0; i < 20; i++ {
		_, err := rand.Read(pub.Key[:])
		if err != nil {
			panic(err)
		}
		acc := types.AccAddress(pub.Address())
		addressPool[i] = acc.String()
	}

	// so we find the top 5 which all the nodes have the show up 3 times, we test with 10 addresses
	testerPool := make([]string, 0, 30)
	for i := 0; i < 10; i++ {
		for j := 0; j < 3; j++ {
			testerPool = append(testerPool, addressPool[i])
		}
	}

	rand2.Shuffle(len(testerPool), func(i, j int) {
		t := testerPool[i]
		testerPool[i] = testerPool[j]
		testerPool[j] = t
	})

	var err error
	in := make([]types.AccAddress, 30, 30)
	for i, el := range testerPool {
		in[i], err = types.AccAddressFromBech32(el)
		if err != nil {
			panic(err)
		}
	}

	a, b := findTopK(in, 5)
	assert.Equal(t, len(a), 10)

	counter := 0
	for _, v := range b {
		counter++
		assert.Equal(t, v, 3)
	}
	assert.Equal(t, counter, 10)

	// now we have 4 addresses that have more than 3 times
	testerPool = append(testerPool, addressPool[1])
	testerPool = append(testerPool, addressPool[2])
	testerPool = append(testerPool, addressPool[3])
	testerPool = append(testerPool, addressPool[4])

	rand2.Shuffle(len(testerPool), func(i, j int) {
		t := testerPool[i]
		testerPool[i] = testerPool[j]
		testerPool[j] = t
	})

	in = make([]types.AccAddress, 34, 34)
	for i, el := range testerPool {
		in[i], err = types.AccAddressFromBech32(el)
		if err != nil {
			panic(err)
		}
	}

	a, b = findTopK(in, 5)
	assert.Equal(t, len(a), 10)

	counter = 0
	for k, v := range b {
		counter++
		if k == addressPool[1] || k == addressPool[2] || k == addressPool[3] || k == addressPool[4] {
			assert.Equal(t, v, 4)
		}
	}
	assert.Equal(t, counter, 10)

	// now we have the first address 5 times
	testerPool = append(testerPool, addressPool[1])

	in = make([]types.AccAddress, 35, 35)
	for i, el := range testerPool {
		in[i], err = types.AccAddressFromBech32(el)
		if err != nil {
			panic(err)
		}
	}

	a, b = findTopK(in, 5)
	assert.Equal(t, len(a), 10)

	counter = 0
	for k, v := range b {
		counter++
		if k == addressPool[2] || k == addressPool[3] || k == addressPool[4] {
			assert.Equal(t, v, 4)
		}
		if k == addressPool[1] {
			assert.Equal(t, v, 5)
		}
	}
	assert.Equal(t, counter, 10)

	// now we add the 5th address
	testerPool = append(testerPool, addressPool[5])

	in = make([]types.AccAddress, 36, 36)
	for i, el := range testerPool {
		in[i], err = types.AccAddressFromBech32(el)
		if err != nil {
			panic(err)
		}
	}

	a, b = findTopK(in, 5)
	assert.Equal(t, len(a), 10)
	counter = 0
	for k, v := range b {
		counter++
		if k == addressPool[2] || k == addressPool[3] || k == addressPool[4] || k == addressPool[5] {
			assert.Equal(t, v, 4)
		}
		if k == addressPool[1] {
			assert.Equal(t, v, 5)
		}
	}
	assert.Equal(t, counter, 10)

	//now will hve only 5 address
	testerPool = append(testerPool, addressPool[1])
	testerPool = append(testerPool, addressPool[1])
	testerPool = append(testerPool, addressPool[1])
	testerPool = append(testerPool, addressPool[1])
	testerPool = append(testerPool, addressPool[1])
	testerPool = append(testerPool, addressPool[1])

	testerPool = append(testerPool, addressPool[2])
	testerPool = append(testerPool, addressPool[2])
	testerPool = append(testerPool, addressPool[2])
	testerPool = append(testerPool, addressPool[2])
	testerPool = append(testerPool, addressPool[2])
	testerPool = append(testerPool, addressPool[2])

	testerPool = append(testerPool, addressPool[3])
	testerPool = append(testerPool, addressPool[3])
	testerPool = append(testerPool, addressPool[3])
	testerPool = append(testerPool, addressPool[3])
	testerPool = append(testerPool, addressPool[3])

	testerPool = append(testerPool, addressPool[4])
	testerPool = append(testerPool, addressPool[4])
	testerPool = append(testerPool, addressPool[4])
	testerPool = append(testerPool, addressPool[4])

	testerPool = append(testerPool, addressPool[5])
	testerPool = append(testerPool, addressPool[5])
	testerPool = append(testerPool, addressPool[5])

	rand2.Shuffle(len(testerPool), func(i, j int) {
		t := testerPool[i]
		testerPool[i] = testerPool[j]
		testerPool[j] = t
	})

	in = make([]types.AccAddress, len(testerPool), len(testerPool))
	for i, el := range testerPool {
		in[i], err = types.AccAddressFromBech32(el)
		if err != nil {
			panic(err)
		}
	}

	a, b = findTopK(in, 5)
	assert.Equal(t, len(a), 5)
	counter = 0
	for k, v := range b {
		counter++

		if k == addressPool[1] {
			assert.Equal(t, v, 11)
		}
		if k == addressPool[2] {
			assert.Equal(t, v, 10)
		}
		if k == addressPool[3] {
			assert.Equal(t, v, 9)
		}

		if k == addressPool[4] {
			assert.Equal(t, v, 8)
		}

		if k == addressPool[5] {
			assert.Equal(t, v, 7)
		}
	}

	// now we have two counter 7
	testerPool = append(testerPool, addressPool[6])
	testerPool = append(testerPool, addressPool[6])
	testerPool = append(testerPool, addressPool[6])
	testerPool = append(testerPool, addressPool[6])
	rand2.Shuffle(len(testerPool), func(i, j int) {
		t := testerPool[i]
		testerPool[i] = testerPool[j]
		testerPool[j] = t
	})

	in = make([]types.AccAddress, len(testerPool), len(testerPool))
	for i, el := range testerPool {
		in[i], err = types.AccAddressFromBech32(el)
		if err != nil {
			panic(err)
		}
	}

	a, b = findTopK(in, 5)
	assert.Equal(t, len(a), 6)
	counter = 0
	for k, v := range b {
		counter++

		if k == addressPool[1] {
			assert.Equal(t, v, 11)
		}
		if k == addressPool[2] {
			assert.Equal(t, v, 10)
		}
		if k == addressPool[3] {
			assert.Equal(t, v, 9)
		}

		if k == addressPool[4] {
			assert.Equal(t, v, 8)
		}

		if k == addressPool[5] || k == addressPool[6] {
			assert.Equal(t, v, 7)
		}
	}
	//now the 6th replace the 5th as it has the most of the addresses
	testerPool = append(testerPool, addressPool[6])
	testerPool = append(testerPool, addressPool[6])
	testerPool = append(testerPool, addressPool[6])
	testerPool = append(testerPool, addressPool[6])
	testerPool = append(testerPool, addressPool[6])
	rand2.Shuffle(len(testerPool), func(i, j int) {
		t := testerPool[i]
		testerPool[i] = testerPool[j]
		testerPool[j] = t
	})

	in = make([]types.AccAddress, len(testerPool), len(testerPool))
	for i, el := range testerPool {
		in[i], err = types.AccAddressFromBech32(el)
		if err != nil {
			panic(err)
		}
	}

	a, b = findTopK(in, 5)
	assert.Equal(t, len(a), 5)
	counter = 0
	for k, v := range b {
		counter++

		if k == addressPool[1] {
			assert.Equal(t, v, 11)
		}
		if k == addressPool[2] {
			assert.Equal(t, v, 10)
		}
		if k == addressPool[3] {
			assert.Equal(t, v, 9)
		}

		if k == addressPool[4] {
			assert.Equal(t, v, 8)
		}

		if k == addressPool[6] {
			assert.Equal(t, v, 12)
		}
	}

}
