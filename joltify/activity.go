package joltify

import (
	"community_tools/tools"
	"encoding/json"
	"fmt"
	"github.com/bwmarrin/discordgo"
)

func (f *Faucet) Activity(session *discordgo.Session, m *discordgo.MessageCreate) {
	userId := m.Author.ID
	storeKey := append([]byte("usertx-"), []byte(userId)...)
	data, err := f.db.QueryDB(storeKey)
	if err != nil {
		errMsg := "fail to query the user activity with error: " + err.Error()
		tools.SendMsg(session, m, errMsg)
	}
	var userActions Actions
	err = json.Unmarshal(data, &userActions)
	if err != nil {
		errMsg := "fail to unmarshal the user activity with error: " + err.Error()
		tools.SendMsg(session, m, errMsg)
	}

	out := fmt.Sprintf(":loudspeaker: for user %v:%v you have done: \n", m.Author.Username, m.Author.ID)
	template := "%v:  :white_check_mark: \n"
	for txType, _ := range userActions.Collect {
		out += fmt.Sprintf(template, txType)
	}

	tools.SendMsg(session, m, out)

	return
}
