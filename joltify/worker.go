package joltify

import (
	"community_tools/tools"
	"encoding/json"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"github.com/rs/zerolog/log"
	"strconv"
	"strings"
)

type Actions struct {
	Collect map[string]bool
}

func (f *Faucet) StoreDiscordUserAction(userid []byte, action string) error {

	storeKey := append([]byte("usertx-"), userid...)
	data, err := f.db.QueryDB(storeKey)
	if err != nil {
		if strings.Contains(err.Error(), "leveldb: not found") {
			userActions := Actions{
				Collect: make(map[string]bool),
			}
			userActions.Collect[action] = true
			data, err := json.Marshal(userActions)
			if err != nil {
				return err
			}
			err = f.db.WriteDB(storeKey, data)
			if err != nil {
				return err
			}
			return nil
		}
		return err
	}
	var userActions Actions
	err = json.Unmarshal(data, &userActions)
	if err != nil {
		return err
	}

	userActions.Collect[action] = true
	data, err = json.Marshal(userActions)
	if err != nil {
		return err
	}
	err = f.db.WriteDB(storeKey, data)
	if err != nil {
		return err
	}
	return nil
}

func (f *Faucet) Collect(session *discordgo.Session, m *discordgo.MessageCreate) {

	auto := false
	if session == nil && m == nil {
		auto = true
	}

	if !auto && m.Author.ID != f.adminID {
		errMsg := "invalid user for the call"
		tools.SendMsg(session, m, errMsg)
		return
	}

	height, err := f.QueryHeightRecord()
	if err != nil {
		errMsg := "error in query height record " + err.Error()
		if auto {
			f.logger.Error().Err(err).Msg(errMsg)
		} else {
			tools.SendMsg(session, m, errMsg)
		}
		return
	}

	var currentHeight uint64
	if height != nil {
		currentHeight, err = strconv.ParseUint(string(height), 10, 64)
		if err != nil {
			log.Error().Msgf("error in parse height %v", err)
			return
		}
	}

	txWithAccount, maxHeight, err := f.QueryUserTx(currentHeight)
	if err != nil {
		errMsg := "error in query user tx " + err.Error()
		if auto {
			f.logger.Error().Err(err).Msg(errMsg)
		} else {
			tools.SendMsg(session, m, errMsg)
		}
		return
	}
	var out string
	if maxHeight != 0 {
		f.ProcessTxAndUpdate(txWithAccount)
		out = fmt.Sprintf("we have processed the txs from height %v to %v", currentHeight, maxHeight)
		if !auto {
			tools.SendMsg(session, m, out)
		} else {
			f.logger.Info().Msg(out)
		}
		return
	}

	out = fmt.Sprintf(":bell:  no new tx to be processed")
	if auto {
		f.logger.Info().Msgf(out)
	} else {
		tools.SendMsg(session, m, out)
	}
}
