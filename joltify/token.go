package joltify

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"time"

	types2 "community_tools/types"

	"community_tools/tools"
	"github.com/bwmarrin/discordgo"
	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/codec"
	codectypes "github.com/cosmos/cosmos-sdk/codec/types"
	"github.com/cosmos/cosmos-sdk/simapp"
	"github.com/cosmos/cosmos-sdk/simapp/params"
	"github.com/cosmos/cosmos-sdk/std"
	"github.com/cosmos/cosmos-sdk/types"
	cosTx "github.com/cosmos/cosmos-sdk/types/tx"
	"github.com/cosmos/cosmos-sdk/types/tx/signing"
	xauthsigning "github.com/cosmos/cosmos-sdk/x/auth/signing"
	"github.com/cosmos/cosmos-sdk/x/auth/tx"
	authtypes "github.com/cosmos/cosmos-sdk/x/auth/types"
	banktypes "github.com/cosmos/cosmos-sdk/x/bank/types"
	grpc1 "github.com/gogo/protobuf/grpc"
)

var chainQueryTimeout = time.Second * 30

func (f *Faucet) GenSendTx(sdkMsg []types.Msg, accSeq, accNum uint64) (client.TxBuilder, error) {
	// Choose your codec: Amino or Protobuf. Here, we use Protobuf, given by the
	// following function.
	encCfg := MakeEncodingConfig()
	// Create a new TxBuilder.
	txBuilder := encCfg.TxConfig.NewTxBuilder()

	err := txBuilder.SetMsgs(sdkMsg...)
	if err != nil {
		return nil, err
	}

	// we use the default here
	txBuilder.SetGasLimit(500000)
	// txBuilder.SetMemo(...)
	// txBuilder.SetTimeoutHeight(...)

	key, err := f.kring.Key("operator")
	if err != nil {
		f.logger.Error().Err(err).Msg("fail to get the operator key")
		return nil, err
	}

	pubkey, err := key.GetPubKey()
	if err != nil {
		f.logger.Error().Err(err).Msg("fail to ge the public key")
		return nil, err
	}

	sigV2 := signing.SignatureV2{
		PubKey: pubkey,
		Data: &signing.SingleSignatureData{
			SignMode:  encCfg.TxConfig.SignModeHandler().DefaultMode(),
			Signature: nil,
		},
		Sequence: accSeq,
	}

	err = txBuilder.SetSignatures(sigV2)
	if err != nil {
		return nil, err
	}

	signerData := xauthsigning.SignerData{
		ChainID:       f.chainID,
		AccountNumber: accNum,
		Sequence:      accSeq,
	}

	signMode := encCfg.TxConfig.SignModeHandler().DefaultMode()
	// Generate the bytes to be signed.
	signBytes, err := encCfg.TxConfig.SignModeHandler().GetSignBytes(signMode, signerData, txBuilder.GetTx())
	if err != nil {
		return nil, err
	}

	signature, pk, err := f.kring.Sign("operator", signBytes)
	if err != nil {
		return nil, err
	}

	// Construct the SignatureV2 struct
	sigData := signing.SingleSignatureData{
		SignMode:  signMode,
		Signature: signature,
	}

	signatureV2 := signing.SignatureV2{
		PubKey:   pk,
		Data:     &sigData,
		Sequence: signerData.Sequence,
	}

	err = txBuilder.SetSignatures(signatureV2)
	if err != nil {
		f.logger.Error().Err(err).Msgf("fail to set the signature")
		return nil, err
	}

	return txBuilder, nil
}

func (f *Faucet) composeAndSend(sendMsg []types.Msg, accSeq, accNum uint64) (bool, string, error) {
	encoding := MakeEncodingConfig()
	txBuilder, err := f.GenSendTx(sendMsg, accSeq, accNum)
	if err != nil {
		f.logger.Error().Err(err).Msg("fail to generate the tx")
		return false, "", err
	}

	ctx, cancel := context.WithTimeout(context.Background(), chainQueryTimeout)
	defer cancel()

	txBytes, err := encoding.TxConfig.TxEncoder()(txBuilder.GetTx())
	if err != nil {
		f.logger.Error().Err(err).Msg("fail to encode the tx")
		return false, "", err
	}

	ok, resp, err := f.BroadcastTx(ctx, txBytes)
	return ok, resp, err
}

// BroadcastTx broadcast the tx to the joltifyChain
func (f *Faucet) BroadcastTx(ctx context.Context, txBytes []byte) (bool, string, error) {
	// Broadcast the tx via gRPC. We create a new client for the Protobuf Tx
	// service.
	txClient := cosTx.NewServiceClient(f.client)
	// We then call the BroadcastTx method on this client.
	grpcRes, err := txClient.BroadcastTx(
		ctx,
		&cosTx.BroadcastTxRequest{
			Mode:    cosTx.BroadcastMode_BROADCAST_MODE_BLOCK,
			TxBytes: txBytes, // Proto-binary of the signed transaction, see previous step.
		},
	)
	if err != nil {
		return false, "", err
	}

	if grpcRes.GetTxResponse().Code != 0 {
		f.logger.Error().Err(err).Msgf("fail to broadcast with response %v", grpcRes.TxResponse)
		return false, "", nil
	}
	txHash := grpcRes.GetTxResponse().TxHash
	return true, txHash, nil
}

func (f *Faucet) SendToken(toAddress string, firstTime bool) error {
	addr, err := types.AccAddressFromBech32(toAddress)
	if err != nil {
		return err
	}

	info, err := f.kring.Key("operator")
	if err != nil {
		f.logger.Error().Err(err).Msg("fail to find the key")
		return err
	}

	senderAddr, err := info.GetAddress()
	if err != nil {
		f.logger.Error().Err(err).Msg("fail to get the address")
		return err
	}

	tokens := f.Tokens
	if firstTime {
		// fixme need to add the bonus token
		//a, err := types.ParseCoinNormalized("1000000ucjolt")
		//if err != nil {
		//	panic(err)
		//}
		//Tokens = Tokens.Add(a)
	}

	// fixme, it seems not working when put two coins in one tx
	msg := banktypes.NewMsgSend(senderAddr, addr, tokens)
	acc, err := f.queryAccount(senderAddr.String(), f.client)
	if err != nil {
		f.logger.Error().Err(err).Msg("fail to query the account")
		return err
	}
	ok, ret, err := f.composeAndSend([]types.Msg{msg}, acc.GetSequence(), acc.GetAccountNumber())
	if err != nil {
		fmt.Printf("fail to send from %v\n", senderAddr.String())
		return err
	}
	if !ok {
		fmt.Printf("fail to send from %v\n", senderAddr.String())
		return errors.New("send failed")
	}
	f.logger.Info().Msgf(" %v we have sent token to %v with %v", ret, addr.String(), tokens.String())
	return nil
}

func genEncodingConfig() params.EncodingConfig {
	cdc := codec.NewLegacyAmino()
	interfaceRegistry := codectypes.NewInterfaceRegistry()
	marshaler := codec.NewProtoCodec(interfaceRegistry)

	return params.EncodingConfig{
		InterfaceRegistry: interfaceRegistry,
		TxConfig:          tx.NewTxConfig(marshaler, tx.DefaultSignModes),
		Amino:             cdc,
	}
}

func MakeEncodingConfig() params.EncodingConfig {
	encodingConfig := genEncodingConfig()
	std.RegisterLegacyAminoCodec(encodingConfig.Amino)
	std.RegisterInterfaces(encodingConfig.InterfaceRegistry)
	simapp.ModuleBasics.RegisterLegacyAminoCodec(encodingConfig.Amino)
	simapp.ModuleBasics.RegisterInterfaces(encodingConfig.InterfaceRegistry)
	return encodingConfig
}

// queryAccount get the current sender account info
func (f *Faucet) queryAccount(addr string, grpcClient grpc1.ClientConn) (authtypes.AccountI, error) {
	accQuery := authtypes.NewQueryClient(grpcClient)
	ctx, cancel := context.WithTimeout(context.Background(), chainQueryTimeout)
	defer cancel()
	accResp, err := accQuery.Account(ctx, &authtypes.QueryAccountRequest{Address: addr})
	if err != nil {
		return nil, err
	}

	encCfg := MakeEncodingConfig()
	var acc authtypes.AccountI
	if err := encCfg.InterfaceRegistry.UnpackAny(accResp.Account, &acc); err != nil {
		return nil, err
	}
	return acc, nil
}

func (f *Faucet) Faucet(session *discordgo.Session, m *discordgo.MessageCreate) {
	invalidAddr := false

	addr, err := types.GetFromBech32(m.Content, "jolt")
	if err != nil {
		invalidAddr = true
	}

	if invalidAddr {
		tools.SendMsg(session, m, " invalid address, please check your address, you need to send the address start with jolt")
		return
	}

	for _, v := range f.reserveAddr {
		if m.Content == v {
			tools.SendMsg(session, m, ":octagonal_sign: reserve address")
			return
		}

	}

	firstTime, err := f.db.ValidationCheck(m.Author.ID, addr)
	if err != nil {
		errMsg := " I'm a bot bot, fail to validate the account as err" + err.Error()
		tools.SendMsg(session, m, errMsg)
		return
	}

	// now we send the token
	f.faucetLocker.Lock()
	err = f.SendToken(m.Content, firstTime)
	f.faucetLocker.Unlock()
	if err != nil {
		errMsg := " fail to send token:" + err.Error()
		tools.SendMsg(session, m, errMsg)
		return
	}

	err = f.db.UpdateDB(m.Author.ID, addr)
	if err != nil {
		errMsg := " update db failed with error: " + err.Error()
		tools.SendMsg(session, m, errMsg)
		return
	}

	_, err = session.ChannelMessageSend(m.ChannelID, ":tada: "+"**"+m.Author.Username+" 100 jolt**"+" has been sent to your wallet.")
	if err != nil {
		fmt.Printf("error in send message %v\n", err)
		return
	}
}

func (f *Faucet) ProcessMessage(session *discordgo.Session, m *discordgo.MessageCreate) {

	if m.Author.ID == session.State.User.ID {
		return
	}

	switch m.ChannelID {
	case types2.FAUCETCHANNEL:
		f.Faucet(session, m)
		return

	case types2.ACTIVITYCHANNEL:

		if strings.HasPrefix(m.Content, "ranking:") {
			f.Ranking(session, m)
			return
		}
		if strings.HasPrefix(m.Content, "activity") {
			f.Activity(session, m)
			return
		}

		if strings.HasPrefix(m.Content, "collect") {
			f.Collect(session, m)
			return
		}
	default:
		return

	}

	//if strings.HasPrefix(m.Content, "invites") {
	//	f.Invites(session, m)
	//	return
	//}

}
