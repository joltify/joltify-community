package joltify

import (
	"community_tools/tools"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"strconv"
)

const INVITATIONPREFIX = "invitation_links_"
const INVITATIONUSERSPREFIX = "invitation_links_users_"

type InvitationLinks struct {
	UserID            string `json:"user_id"`
	InvitationCounter int    `json:"invitation_link"`
}

func (f *Faucet) LoadUserPreviousInvite(userID string) (int64, error) {
	key := append([]byte(INVITATIONPREFIX), []byte(userID)...)
	data, err := f.db.QueryDB(key)
	if err != nil {
		// this means no record found
		if err.Error() == "leveldb: not found" {
			return -1, nil
		}
		return 0, err
	}

	usersCount, err := strconv.ParseInt(string(data), 10, 64)
	return usersCount, err
}

func (f *Faucet) SaveUserInvite(userID string, usersCount uint64) error {
	counter := strconv.FormatUint(usersCount, 10)
	key := append([]byte(INVITATIONPREFIX), []byte(userID)...)
	return f.db.WriteDB(key, []byte(counter))
}

func (f *Faucet) QueryUserInvites(userID string) ([]string, error) {
	key := append([]byte(INVITATIONUSERSPREFIX), []byte(userID)...)
	fmt.Printf("query key is %v\n", string(key))
	data, err := f.db.QueryDB(key)
	if err != nil {
		if err.Error() == "leveldb: not found" {
			fmt.Printf("query not found\n")
			return nil, nil
		}
		return nil, err
	}
	var users map[string]bool
	err = json.Unmarshal(data, &users)
	if err != nil {
		return nil, err
	}

	var userList []string
	for k := range users {
		userList = append(userList, k)
	}
	return userList, nil
}

func (f *Faucet) AddUserInvited(userID string, invited string) error {
	key := append([]byte(INVITATIONUSERSPREFIX), []byte(userID)...)

	data, err := f.db.QueryDB(key)
	if err != nil {
		if err.Error() == "leveldb: not found" {
			users := make(map[string]bool)
			users[invited] = true
			data, err := json.Marshal(users)
			if err != nil {
				return err
			}
			return f.db.WriteDB(key, data)
		}
		return err
	}

	var userList map[string]bool
	err = json.Unmarshal(data, &userList)
	if err != nil {
		return err
	}
	userList[invited] = true
	data, err = json.Marshal(userList)
	if err != nil {
		return err
	}
	return f.db.WriteDB(key, data)
}

func (f *Faucet) RemoveUserInvited(userID string, invited string) error {
	key := append([]byte(INVITATIONUSERSPREFIX), []byte(userID)...)
	data, err := f.db.QueryDB(key)
	if err != nil {
		if err.Error() == "leveldb: not found" {
			return errors.New("no record found")
		}
		return err
	}

	var userList map[string]bool
	err = json.Unmarshal(data, &userList)
	if err != nil {
		return err
	}

	delete(userList, invited)
	data, err = json.Marshal(userList)
	if err != nil {
		return err
	}
	return f.db.WriteDB(key, data)
}

func (f *Faucet) ProcessEventAddMember(session *discordgo.Session, user *discordgo.GuildMemberAdd) {

	allChannels, err := session.ChannelInvites(f.ChannelID, discordgo.WithContext(context.Background()))
	if err != nil {
		f.logger.Error().Err(err).Msgf("fail to get all channels")
		return
	}

	for _, el := range allChannels {
		loadAllUsersInvitation, err := f.LoadUserPreviousInvite(el.Inviter.ID)
		if err != nil {
			f.logger.Error().Err(err).Msgf("fail to load the user previous invitation")
			continue
		}
		if loadAllUsersInvitation == -1 {
			err = f.SaveUserInvite(el.Inviter.ID, uint64(el.Uses))
			if err != nil {
				f.logger.Error().Err(err).Msgf("fail to save the user invitation")
				continue
			}
			err = f.AddUserInvited(el.Inviter.ID, user.User.ID)
			if err != nil {
				f.logger.Error().Err(err).Msgf("fail to add the user to the invited list for new record")
				continue
			}
		}
		if loadAllUsersInvitation < int64(el.Uses) {
			err = f.AddUserInvited(el.Inviter.ID, user.User.ID)
			if err != nil {
				f.logger.Error().Err(err).Msgf("fail to add the user to the invited list")
				continue
			}
			err = f.SaveUserInvite(el.Inviter.ID, uint64(el.Uses))
			if err != nil {
				f.logger.Error().Err(err).Msgf("fail to save the user invitation")
				continue
			}
		}
	}
	return
}

func (f *Faucet) ProcessEventRemoveMember(session *discordgo.Session, user *discordgo.GuildMemberRemove) {

	allChannels, err := session.ChannelInvites(f.ChannelID, discordgo.WithContext(context.Background()))
	if err != nil {
		f.logger.Error().Err(err).Msgf("fail to get all channels")
		return
	}

	for _, el := range allChannels {
		loadAllUsersInvitation, err := f.LoadUserPreviousInvite(el.Inviter.ID)
		if err != nil {
			f.logger.Error().Err(err).Msgf("fail to load the user previous invitation")
			continue
		}
		if loadAllUsersInvitation == -1 {
			// no record found, we continue
			f.logger.Info().Msgf("no record found for the user %v", el.Inviter.ID)
			continue
		}
		err = f.RemoveUserInvited(el.Inviter.ID, user.User.ID)
		if err != nil {
			f.logger.Error().Err(err).Msgf("fail to delete the user to the invited list")
			continue
		}
		err = f.SaveUserInvite(el.Inviter.ID, uint64(el.Uses))
		if err != nil {
			f.logger.Error().Err(err).Msgf("fail to save the user invitation")
			continue
		}
	}
	return
}

func (f *Faucet) Invites(session *discordgo.Session, m *discordgo.MessageCreate) {
	users, err := f.QueryUserInvites(m.Author.ID)
	if err != nil {
		errMsg := "fail to query user invites " + err.Error()
		tools.SendMsg(session, m, errMsg)
		return
	}
	if users == nil {
		tools.SendMsg(session, m, "no users found")
		return
	}
	if len(users) > 10 {
		userStr := ""
		for i := 0; i < 10; i++ {
			userStr += users[i] + ","
		}
		out := fmt.Sprintf("total %v users and the first 10 users are %v", len(users), userStr)
		tools.SendMsg(session, m, out)
		return
	}
	userStr := ""
	for _, el := range users {
		userStr += el + ","
	}
	out := fmt.Sprintf("total %v users and the users are %v", len(users), userStr)
	tools.SendMsg(session, m, out)
	return
}
