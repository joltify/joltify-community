package joltify

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"community_tools/db"
	"github.com/cosmos/cosmos-sdk/codec"
	codectypes "github.com/cosmos/cosmos-sdk/codec/types"
	cryptocodec "github.com/cosmos/cosmos-sdk/crypto/codec"
	"github.com/cosmos/cosmos-sdk/crypto/keyring"
	"github.com/cosmos/cosmos-sdk/types"
	grpc1 "github.com/gogo/protobuf/grpc"
	"github.com/rs/zerolog"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ConfigFaucet struct {
	Port            string `yaml:"port" env:"PORT" env-default:"1317"`
	Host            string `yaml:"host" env:"HOST" env-default:"localhost"`
	ChainID         string `yaml:"chainID" env:"CHAINID" env-default:"joltifydev_1730-1"`
	TOKENS          string `yaml:"tokens" env:"tokens" env-default:"100jolt,10000000000000000000abusd,1000000000000000000abnb"`
	KeyringPassword string `yaml:"password" env:"PASSWORD"`
	DisCordToken    string `yaml:"discord_token" env:"DISCORDTOKEN"`
	ChannelID       string `yaml:"channel_id" env:"CHANNELID"`
	HasuraURL       string `yaml:"hasura_url" env:"HASURAURL"`
	TopK            string `yaml:"top_k" env:"TOPK" env-default:"10"`
	AdminID         string `yaml:"admin_id" env:"ADMINID"`
	ReserveAddr     string `yaml:"reserve_addr" env:"RESERVEADDR"`
	HttpAddr        string `yaml:"http_addr" env:"HTTPADDR" env-default:"localhost:26657"`
	RESTAddr        string `yaml:"rest_addr" env:"RESTADDR" env-default:"localhost:1317"`
}

type Faucet struct {
	logger       zerolog.Logger
	client       grpc1.ClientConn
	kring        keyring.Keyring
	chainID      string
	Tokens       types.Coins
	db           *db.LevelDB
	faucetLocker *sync.RWMutex
	ChannelID    string
	hasuraUrl    string
	topK         int
	adminID      string
	reserveAddr  []string
	restAddr     string
}

type AuctionElement struct {
	Type        string `json:"@type"`
	BaseAuction struct {
		ID        string `json:"id"`
		Initiator string `json:"initiator"`
		Lot       struct {
			Denom  string `json:"denom"`
			Amount string `json:"amount"`
		} `json:"lot"`
		Bidder string `json:"bidder"`
		Bid    struct {
			Denom  string `json:"denom"`
			Amount string `json:"amount"`
		} `json:"bid"`
		HasReceivedBids bool      `json:"has_received_bids"`
		EndTime         time.Time `json:"end_time"`
		MaxEndTime      time.Time `json:"max_end_time"`
	} `json:"base_auction"`
}

type AuctionDetail struct {
	Data AuctionElement `json:"auction"`
}

type AllAuctions struct {
	Auctions   []AuctionElement `json:"auctions"`
	Pagination struct {
		NextKey any    `json:"next_key"`
		Total   string `json:"total"`
	} `json:"pagination"`
}

func getCodec() codec.Codec {
	registry := codectypes.NewInterfaceRegistry()
	cryptocodec.RegisterInterfaces(registry)
	return codec.NewProtoCodec(registry)
}

func InitFaucet(cfg ConfigFaucet) (*Faucet, error) {

	grpcAddr := fmt.Sprintf("%s:%s", cfg.Host, cfg.Port)
	coins, err := types.ParseCoinsNormalized(cfg.TOKENS)
	if err != nil {
		return nil, err
	}

	reserveAddr := strings.Split(cfg.ReserveAddr, ",")

	senderKey := keyring.NewInMemory(getCodec())
	dat, err := os.ReadFile("./alice.key")
	if err != nil {
		log.Fatalln("error in read keyring file")
		return nil, err
	}
	err = senderKey.ImportPrivKey("operator", string(dat), "12345678")
	if err != nil {
		fmt.Printf("fail to open the keyfile")
		return nil, err
	}

	// Deprecated: use WithTransportCredentials and insecure.NewCredentials()
	tc := insecure.NewCredentials()
	client, err := grpc.Dial(grpcAddr, grpc.WithTransportCredentials(tc))
	if err != nil {
		return nil, err
	}
	myDb := db.OpenDB()

	topKD, err := strconv.ParseInt(cfg.TopK, 10, 32)
	if err != nil {
		return nil, err
	}

	f := Faucet{
		zerolog.New(os.Stdout).With().Timestamp().Logger(),
		client,
		senderKey,
		cfg.ChainID,
		coins,
		&myDb,
		&sync.RWMutex{},
		cfg.ChannelID,
		cfg.HasuraURL,
		int(topKD),
		cfg.AdminID,
		reserveAddr,
		cfg.RESTAddr,
	}

	return &f, nil
}
