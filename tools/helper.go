package tools

import (
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
)

func SendMsg(session *discordgo.Session, m *discordgo.MessageCreate, msg string) {
	_, err := session.ChannelMessageSend(m.ChannelID, msg)
	if err != nil {
		fmt.Printf("error in send message %v\n", err)
		return
	}
}

func TimeConvert(in string) (time.Time, error) {
	// Define the layout of the input string
	layout := "2006-01-02 15:04:05.999999999 -0700 MST"

	// Parse the string into a time.Time value
	parsedTime, err := time.Parse(layout, in)
	if err != nil {
		fmt.Println("Error:", err)
		return time.Time{}, err
	}
	return parsedTime, nil
}
