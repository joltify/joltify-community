package tools

import (
	"github.com/cosmos/cosmos-sdk/crypto/keys/secp256k1"
	"github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/bech32/legacybech32" //nolint
)

// SetupBech32Prefix sets up the prefix of the joltify chain
func init() {
	config := types.GetConfig()
	config.SetBech32PrefixForAccount("jolt", "joltpub")
	config.SetBech32PrefixForValidator("joltvaloper", "joltvpub")
	config.SetBech32PrefixForConsensusNode("joltvalcons", "joltcpub")
}

// PoolPubKeyToJoltifyAddress return the joltify encoded pubkey
func PubKeyToJoltifyAddress(pk string) (types.AccAddress, error) {
	pubkey, err := legacybech32.UnmarshalPubKey(legacybech32.AccPK, pk) //nolint
	if err != nil {
		return types.AccAddress{}, err
	}

	addr, err := types.AccAddressFromHexUnsafe(pubkey.Address().String())
	return addr, err
}

func PubkeyToJoltAddrByte(pkBytes []byte) types.AccAddress {
	pk := secp256k1.PubKey{
		Key: pkBytes,
	}
	return pk.Address().Bytes()
}
