package grpc

import (
	"context"
	"fmt"
	"github.com/rs/zerolog"
	tmclienthttp "github.com/tendermint/tendermint/rpc/client/http"
	coretypes "github.com/tendermint/tendermint/rpc/core/types"
	"os"
	"sync"
	"time"
)

const (
	capacity = 10000
)

type JoltifyNode struct {
	client       *tmclienthttp.HTTP
	logger       zerolog.Logger
	BidChan      chan coretypes.ResultEvent
	CloseBidChan chan coretypes.ResultEvent
	ChannelWg    *sync.WaitGroup
}

func NewCosOperations(httpAddr string) (*JoltifyNode, error) {
	var err error

	client, err := tmclienthttp.New(httpAddr, "/websocket")
	if err != nil {
		return nil, err
	}
	err = client.Start()
	if err != nil {
		return nil, err
	}

	node := JoltifyNode{
		client:       client,
		logger:       zerolog.New(os.Stdout).With().Timestamp().Logger(),
		ChannelWg:    &sync.WaitGroup{},
		CloseBidChan: make(chan coretypes.ResultEvent, capacity),
		BidChan:      make(chan coretypes.ResultEvent, capacity),
	}

	return &node, nil

}

func (jn *JoltifyNode) TerminateCosmosClient() error {
	if jn.client != nil {
		err := jn.client.Stop()
		if err != nil {
			jn.logger.Error().Err(err).Msg("fail to terminate the ws")
		}
	}
	return nil
}

func (jn *JoltifyNode) AddSubscribe(ctx context.Context, auctionID string) error {

	jn.ChannelWg.Add(2)
	go func() {
		defer jn.ChannelWg.Done()
		ctxTimeout, cancel := context.WithTimeout(ctx, time.Second*10)
		defer cancel()
		queryClose := fmt.Sprintf("auction_close.auction_id=%s", auctionID)
		closeBidChan, err := jn.client.Subscribe(ctxTimeout, "auction_close_"+auctionID, queryClose, 10)
		if err != nil {
			fmt.Printf("fail to start the subscription")
			return
		}

		for {
			select {
			case <-ctx.Done():
				return
			case msg := <-closeBidChan:
				jn.CloseBidChan <- msg
				continue
			}
		}
	}()

	go func() {
		defer jn.ChannelWg.Done()

		ctxTimeout, cancel := context.WithTimeout(ctx, time.Second*10)
		defer cancel()
		queryBid := fmt.Sprintf("auction_bid.auction_id=%s", auctionID)
		bidChan, err := jn.client.Subscribe(ctxTimeout, "auction_bid_"+auctionID, queryBid, 10)
		//closeBidChan, err := jn.client.Subscribe(ctxTimeout, auctionID, "auction_bid.auction_id=18", 1)
		if err != nil {
			fmt.Printf("fail to start the subscription")
			return
		}

		for {
			select {
			case <-ctx.Done():
				return
			case msg := <-bidChan:
				jn.BidChan <- msg
				continue
			}
		}
	}()

	return nil
}

func (jn *JoltifyNode) RemoveSubscribe(ctx context.Context, auctionID string) error {
	fmt.Printf(">>>>>>>>>>>>>remove>>>>>>event>>>>>>>>>\n")
	queryClose := fmt.Sprintf("auction_close.auction_id = '%s'", auctionID)
	queryBid := fmt.Sprintf("auction_bid.auction_id=%s", auctionID)
	err := jn.client.Unsubscribe(ctx, "auction_close_"+auctionID, queryClose)
	if err != nil {
		jn.logger.Error().Err(err).Msg("fail to unsubscribe the auction close")
	}
	err = jn.client.Unsubscribe(ctx, "auction_bid_"+auctionID, queryBid)
	return err
}
