package txstatistic

import (
	"os"
	"strings"

	chart "github.com/wcharczuk/go-chart/v2"
)

func barchartTemplate() chart.BarChart {
	graph := chart.BarChart{
		Title: "Tx distribution",
		Background: chart.Style{
			Padding: chart.Box{
				Top: 40,
			},
		},
		Height:   512,
		BarWidth: 60,
		Bars:     []chart.Value{},
	}

	return graph
}

func Draw(freq map[string]int) error {
	f, _ := os.Create("output.png")
	defer f.Close()
	graph := barchartTemplate()

	for k, v := range freq {
		labeles := strings.Split(k, ".")
		graph.Bars = append(graph.Bars, chart.Value{Value: float64(v), Label: labeles[len(labeles)-1]})
	}

	err := graph.Render(chart.PNG, f)
	return err
}
