package txstatistic

import (
	"community_tools/tools"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/cosmos/cosmos-sdk/types"
)

type SignerInfo struct {
	Sequence string `json:"sequence"`
	ModeInfo struct {
		Single struct {
			Mode string `json:"mode"`
		} `json:"single"`
	} `json:"mode_info"`
	PublicKey struct {
		Key  string `json:"key"`
		Type string `json:"@type"`
	} `json:"public_key"`
}

type MessageOnlyType struct {
	Type string `json:"@type"`
}

type AuctionMsg struct {
	Type   string `json:"@type"`
	Amount struct {
		Denom  string `json:"denom"`
		Amount string `json:"amount"`
	} `json:"amount"`
	Bidder    string `json:"bidder"`
	AuctionID string `json:"auction_id"`
}

type TxUserInfos struct {
	Transaction []struct {
		SignerInfos []SignerInfo `json:"signer_infos"`
		Messages    interface{}  `json:"messages"`
		Height      uint64       `json:"height"`
	} `json:"transaction"`
}

func getAlladdresses(in TxUserInfos) []types.AccAddress {
	var addrs []types.AccAddress
	for _, tx := range in.Transaction {
		pk := tx.SignerInfos[0].PublicKey.Key
		out, err := base64.StdEncoding.DecodeString(pk)
		if err != nil {
			panic("fail to get the pubkey from base64 encoding")
		}
		addr := tools.PubkeyToJoltAddrByte(out)
		addrs = append(addrs, addr)
	}
	return addrs
}

func globalAddressRanking(addrs []types.AccAddress) (map[string]int, map[int][]string) {
	addrFrq := make(map[string]int)
	for _, el := range addrs {
		freq, ok := addrFrq[el.String()]
		if !ok {
			addrFrq[el.String()] = 1
		} else {
			addrFrq[el.String()] = freq + 1
		}
	}

	freqAddr := make(map[int][]string)

	for addr, freq := range addrFrq {
		addresses, ok := freqAddr[freq]
		if !ok {
			freqAddr[freq] = []string{addr}
		} else {
			freqAddr[freq] = append(addresses, addr)
		}

	}

	return addrFrq, freqAddr
}

func putInStore(txTypes map[string][]types.AccAddress, eachTxType string, addr types.AccAddress) {
	store, ok := txTypes[eachTxType]
	if ok {
		store = append(store, addr)
		txTypes[eachTxType] = store
		return
	}
	newStore := make([]types.AccAddress, 1, 100)
	newStore[0] = addr
	txTypes[eachTxType] = newStore
}

func categoryProcess(txs TxUserInfos, txTypes map[string][]types.AccAddress, txType string) (map[string]int, map[string][]types.AccAddress, uint64) {
	var maxHeight uint64
	for _, tx := range txs.Transaction {
		if tx.Height > maxHeight {
			maxHeight = tx.Height
		}
		//eachTxTypeStr := strings.Split(tx.Messages[0].Type, ".")
		//eachTxType := eachTxTypeStr[len(eachTxTypeStr)-1]

		jsonData, err := json.Marshal(tx.Messages)
		if err != nil {
			panic(err)
		}

		var msg []MessageOnlyType
		err = json.Unmarshal(jsonData, &msg)
		if err != nil {
			panic(err)
		}
		eachTxType := msg[0].Type

		if txType == "all" {
			pk := tx.SignerInfos[0].PublicKey.Key
			out, err := base64.StdEncoding.DecodeString(pk)
			if err != nil {
				panic("fail to get the pubkey from base64 encoding")
			}
			addr := tools.PubkeyToJoltAddrByte(out)
			putInStore(txTypes, eachTxType, addr)
			continue
		}

		if txType == eachTxType {
			pk := tx.SignerInfos[0].PublicKey.Key
			out, err := base64.StdEncoding.DecodeString(pk)
			if err != nil {
				panic("fail to get the pubkey from base64 encoding")
			}
			addr := tools.PubkeyToJoltAddrByte(out)
			putInStore(txTypes, eachTxType, addr)
			continue
		}

	}
	freq := make(map[string]int)

	for k, v := range txTypes {
		freq[k] = len(v)
	}
	return freq, txTypes, maxHeight
}

func DecodeTx(ret interface{}) (TxUserInfos, error) {
	jsonData, err := json.Marshal(ret)
	if err != nil {
		fmt.Println("Failed to marshal JSON:", err)
		return TxUserInfos{}, err
	}

	var txInfo TxUserInfos
	err = json.Unmarshal(jsonData, &txInfo)
	if err != nil {
		fmt.Printf("fail to unmarshal the info")
		return txInfo, err
	}
	return txInfo, nil

}

func TxStatistic(txInfo TxUserInfos, txType string) (map[string]int, map[string][]types.AccAddress, map[int][]string, uint64, error) {

	address := getAlladdresses(txInfo)
	_, globalFreqAddr := globalAddressRanking(address)

	txTypes := make(map[string][]types.AccAddress)
	txFreq, txwithWalletsArray, maxHeight := categoryProcess(txInfo, txTypes, txType)

	return txFreq, txwithWalletsArray, globalFreqAddr, maxHeight, nil
}
