package types

import (
	"time"

	"github.com/cosmos/cosmos-sdk/types"
)

const (
	FAUCETCHANNEL   = "1148458157891387512"
	ACTIVITYCHANNEL = "1163712347005792287"
	BIDCHANNEL      = "1163968348397260830"
)

type WalletEntry struct {
	Timestamp       time.Time
	WalletAddresses []types.AccAddress
	FirstJoin       bool
	Reserve         []byte
}
