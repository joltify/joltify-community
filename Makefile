community:
	@go build -o community ./cmd/tx_statistic/cmd.go ./cmd/tx_statistic/draw.go
	@echo "compile the community tool done"

faucet:
	@go build -o faucet ./cmd/bot/faucet.go
	@echo "compile the faucet done"

tool:
	@go build -o faucet ./cmd/bot/main.go
	@echo "compile the faucet done"


format:
	@gofumpt -l -w .

lint:
	@golangci-lint run --out-format=tab  -v --timeout 3600s -c ./.golangci.yml
	go mod verify

clean:
	rm -rf community faucet
